package ru.aint.mtg.toolbox.ui;

import java.io.IOException;
import java.util.ArrayList;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.DraftConfig;
import ru.aint.mtg.toolbox.data.providers.DraftTimeConfigProvider;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Activity that lists all saved draft time configuration.
 */
public class DraftTimeConfigListActivity extends ListActivity {

    /**
     * Result code for the activity.
     */
    public static int DRAFT_TIME_CONFIG_LIST_RESULT_CODE = 1011;

    /**
     * Result bundle key.
     */
    public static String DRAFT_TIME_CONFIG_LIST_RESULT_KEY = "draft_time_config_list_result";
    
    /**
     * List view.
     */
    ListView mList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.draft_time_config_list_activity);

        mList = (ListView)findViewById(android.R.id.list);

        mList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemView, int itemIndex, long id) {
                View listItem = mList.getChildAt(itemIndex);
                DraftConfigListAdapter.ConfigHolder holder = (DraftConfigListAdapter.ConfigHolder)listItem.getTag();
                DraftConfig config = holder.config;

                Bundle resultBundle = new Bundle();
                resultBundle.putSerializable(DRAFT_TIME_CONFIG_LIST_RESULT_KEY, config);
                Intent intent = new Intent();
                intent.putExtras(resultBundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        
        mList.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemView,
                    int itemIndex, long id) {
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadList();
    }

    private void loadList() {
        DraftTimeConfigProvider provider = new DraftTimeConfigProvider(this);
        try {
            DraftConfig[] configs = provider.getConfigs();
            mList.setAdapter(new DraftConfigListAdapter(this, R.layout.draft_config_list_item, configs));
        } catch (IOException e) {
            Log.d("DEBUG", "Failed to load configs: " + e.getMessage());
        }
    }

    /**
     *  List adapter.
     */
    private class DraftConfigListAdapter extends ArrayAdapter<DraftConfig> {

        /**
         * Helper class for Plugin views.
         */
        public class ConfigHolder {
            public DraftConfig config;
            public TextView titleView;
            public TextView descriptionView;
            public TextView baseTimeView;
        }

        /**
         * Array of items.
         */
        private ArrayList<DraftConfig> mConfigs;

        /**
         * Application context.
         */
        private Context mContext;

        /**
         * Title typeface.
         */
        private Typeface mTitleTypeface = null;

        /**
         * Description typeface.
         */
        private Typeface mDescriptionTypeface = null;

        /**
         * Resource id for list item view.
         */
        private int mViewResourceId;

        /**
         * Public constructor.
         * 
         * @param context Application context.
         * @param textViewResourceId Resource id for list item view.
         * @param objects Array of objects to display.
         */
        public DraftConfigListAdapter(Context context, int textViewResourceId,
                DraftConfig[] objects) {
            super(context, textViewResourceId, objects);
            mContext = context;
            mViewResourceId = textViewResourceId;

            mConfigs = new ArrayList<DraftConfig>();
            if (objects != null) {
                for (DraftConfig config : objects) {
                    mConfigs.add(config);
                }
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ConfigHolder holder = null;

            if(row == null) {
                LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
                row = inflater.inflate(mViewResourceId, parent, false);

                holder = new ConfigHolder();
                holder.config = mConfigs.get(position);
                holder.titleView = (TextView)row.findViewById(R.id.title_text_view);
                holder.descriptionView = (TextView)row.findViewById(R.id.description_text_view);
                holder.baseTimeView = (TextView)row.findViewById(R.id.base_time_text_view);

                row.setTag(holder);
            } else {
                holder = (ConfigHolder)row.getTag();
            }

            DraftConfig config = mConfigs.get(position);
            holder.titleView.setTypeface(getTitleTypeface());
            holder.titleView.setText(config.getName());
            holder.descriptionView.setText(config.getDescription());
            holder.descriptionView.setTypeface(getDescriptionTypeface());
            holder.baseTimeView.setTypeface(getDescriptionTypeface());
            holder.baseTimeView.setText(String.format("%.0f", config.getTimingForPick(0)));

            return row;
        }

        /**
         * Returns typeface for title view.
         * 
         * @return Typeface for title view.
         */
        private Typeface getTitleTypeface() {
            if (mTitleTypeface == null) {
                try {
                    mTitleTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/CenturyGothicBold.ttf");
                } catch (Exception ex) {
                    Log.d("DEBUG", "error: " + ex.getMessage());
                }
            }
            return mTitleTypeface;
        }

        /**
         * Returns typeface for description view.
         * 
         * @return Typeface for description view.
         */
        private Typeface getDescriptionTypeface() {
            if (mDescriptionTypeface == null) {
                try {
                    mDescriptionTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/CenturyGothicRegular.ttf");
                } catch (Exception ex) {
                    Log.d("DEBUG", "error: " + ex.getMessage());
                }
            }
            return mDescriptionTypeface;
        }
    }
}
