package ru.aint.mtg.toolbox.ui;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.plugins.BasePlugin;
import ru.aint.mtg.toolbox.data.providers.HubActivitiesProvider;
import ru.aint.mtg.toolbox.ui.components.PluginArrayAdapter;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Main hub activity to launch other activities.
 * @author aint
 *
 */
public class HubActivity extends Activity {

    /**
     * Activity list view.
     */
    ListView mActivityList;
    
    /**
     * Plugin list adapter.
     */
    PluginArrayAdapter mAdapter;
    
    /**
     * Plugin items.
     */
    BasePlugin[] mPlugins;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hub_activity);
        
        mActivityList = (ListView)findViewById(R.id.activities_list);
        initList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hub_activity, menu);
        return true;
    }

    /**
     * Setups list view to display activities.
     */
    protected void initList() {
        HubActivitiesProvider provider = new HubActivitiesProvider(getApplicationContext());
        mPlugins = provider.getActivities();
        mAdapter = new PluginArrayAdapter(this, R.layout.plugin_list_item, mPlugins);
        mActivityList.setAdapter(mAdapter);
        
        mActivityList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemView, int itemIndex, long id) {
                BasePlugin plugin = mPlugins[itemIndex];
                plugin.execute(HubActivity.this);                
            }
        });
        mActivityList.setOnTouchListener(new ListTouchListener());
    }
    
    /**
     * List item touch handler. Used to highlight accessory view.
     */
    class ListTouchListener implements OnTouchListener {

        /**
         * Initial position for the touch down event.
         */
        int mDownPosition = -1;
        
        /**
         * Item that was clicked.
         */
        View mItem = null;
        
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            
            int action = event.getActionMasked();
            View child = null;
            
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    mDownPosition = mActivityList.pointToPosition((int)event.getX(), (int)event.getY());
                    child = ListView.INVALID_POSITION != mDownPosition ? mActivityList.getChildAt(mDownPosition) : null;
                    if (child != null) {
                        mItem = child;
                        PluginArrayAdapter.PluginHolder holder = (PluginArrayAdapter.PluginHolder)mItem.getTag();
                        holder.arrowView.setImageDrawable(getResources().getDrawable(R.drawable.accessory_arrow_selected));
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    int position = mDownPosition - mActivityList.getFirstVisiblePosition();
                    child = ListView.INVALID_POSITION != mDownPosition ? mActivityList.getChildAt(position) : null;
                    if (child != null) {
                        PluginArrayAdapter.PluginHolder holder = (PluginArrayAdapter.PluginHolder)mItem.getTag();
                        holder.arrowView.setImageDrawable(getResources().getDrawable(R.drawable.accessory_arrow));
                        child.setPressed(false);
                    }
                    break;
                case MotionEvent.ACTION_OUTSIDE:
                    if (mItem != null) {
                        PluginArrayAdapter.PluginHolder holder = (PluginArrayAdapter.PluginHolder)mItem.getTag();
                        holder.arrowView.setImageDrawable(getResources().getDrawable(R.drawable.accessory_arrow));
                        mItem.setPressed(false);
                    }
                    break;
                default:
                    break;
            }
            
            return false;
        }
        
    }
}
