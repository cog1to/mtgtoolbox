package ru.aint.mtg.toolbox.ui;

import java.util.ArrayList;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.Configuration;
import ru.aint.mtg.toolbox.data.LifeCounter;
import ru.aint.mtg.toolbox.preferences.Preferences;
import ru.aint.mtg.toolbox.ui.components.LifeCounterView;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

/**
 * Life Counter activity.
 */
public class LifeCounterActivity extends Activity {

    /**
     * Root layout widget.
     */
    LinearLayout mRootLayout;

    /**
     * Reset life button.
     */
    ImageButton mResetLifeButton;

    /**
     * List of tracked life counters.
     */
    ArrayList<LifeCounter> mLifeCounters;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.life_counter_activity);

        mRootLayout = (LinearLayout)findViewById(R.id.root_layout);
        mResetLifeButton = (ImageButton)findViewById(R.id.reset_life_button);
        mResetLifeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (mLifeCounters != null) {
                        for (LifeCounter counter : mLifeCounters) {
                            counter.setLifeTotal(Configuration.DEFAULT_START_LIFE_COUNT);
                        }
                    }
                } catch (Exception e) {
                    Log.e("MTGToolbox", "mResetLifeButton Error: " + e.getMessage());
                }
            }
        });
    }

    /**
     * Creates default views.
     */
    private void initForTwoPlayers() {
        mLifeCounters = new ArrayList<LifeCounter>();
        mLifeCounters.add(new LifeCounter());
        mLifeCounters.add(new LifeCounter());

        setupUI();
    }

    @Override
    protected void onPause() {
        try {
            LifeCounter[] counters = new LifeCounter[mLifeCounters.size()];
            mLifeCounters.toArray(counters);
            Preferences.LifeCounterPreferences.setLastSessionCounters(getApplicationContext(), counters);
        } catch (Exception e) {
            Log.d("MTGToolbox", "Error while saving life counters state");
        }
        
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            LifeCounter[] counters = Preferences.LifeCounterPreferences.getLastSessionCounters(getApplicationContext());
            if ((counters != null) && (counters.length > 0)) {
                mLifeCounters = new ArrayList<LifeCounter>();
                for (LifeCounter counter : counters) {
                    mLifeCounters.add(counter);
                }
                setupUI();
            } else {
                initForTwoPlayers();
            }
        }
        catch (Exception e) {
            initForTwoPlayers();
        }
    }

    protected void setupUI() {
        LifeCounterView view1 = (LifeCounterView)findViewById(R.id.counter_view_1);
        LifeCounterView view2 = (LifeCounterView)findViewById(R.id.counter_view_2);

        mLifeCounters.get(0).addOnChangeListener(view1);
        view1.getLeftButton().setOnClickListener(new SpinButtonListener(mLifeCounters.get(0), true));
        view1.getRightButton().setOnClickListener(new SpinButtonListener(mLifeCounters.get(0), false));

        mLifeCounters.get(1).addOnChangeListener(view2);
        view2.getLeftButton().setOnClickListener(new SpinButtonListener(mLifeCounters.get(1), true));
        view2.getRightButton().setOnClickListener(new SpinButtonListener(mLifeCounters.get(1), false));
    }

    /**
     * Class that processes spin buttons clicks.
     */
    protected class SpinButtonListener implements OnClickListener {

        /**
         * Life counter value holder.
         */
        private LifeCounter mLifeCounter;

        /**
         * Flag to indicate that this listener must decrease life totals.
         */
        private boolean mIsDecreasing = false;

        /**
         * Main constructor.
         * 
         * @param counter Life counter to watch.
         */
        public SpinButtonListener(LifeCounter counter) {
            mLifeCounter = counter;
        }

        /**
         * Constructor with decreasing flag.
         * 
         * @param counter Counter object.
         * 
         * @param lifeView Life counter view to display life total.
         * 
         * @param isDecreasing Flag indicating if life should decrease on click.
         */
        public SpinButtonListener(LifeCounter counter, boolean isDecreasing) {
            this(counter);
            mIsDecreasing = isDecreasing;
        }

        @Override
        public void onClick(View v) {
            if (mIsDecreasing) {
                mLifeCounter.decreaseLifeTotal();
            } else {
                mLifeCounter.increaseLifeTotal();
            }            
        }
    }
}
