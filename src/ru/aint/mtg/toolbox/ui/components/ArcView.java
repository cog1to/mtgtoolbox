package ru.aint.mtg.toolbox.ui.components;

import ru.aint.mtg.toolbox.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class ArcView extends View {

    /**
     * Arc color.
     */
    int mColor;
    
    /**
     * Angle from which to start arc.
     */
    float mStartAngle;
    
    /**
     * Length of the arc.
     */
    float mSwipeAngle;
    
    public ArcView(Context context) {
        super(context);
        init(context);
    }

    public ArcView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ArcView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    protected void init(Context context) {
        mStartAngle = -90;
        mSwipeAngle = 0;
        mColor = context.getResources().getColor(R.color.timer_arc_color);
    }
    
    /**
     * Returns arc color.
     * 
     * @return Arc color object.
     */
    public int getColor() {
        return mColor;
    }
    
    /**
     * Sets arc color.
     * 
     * @param color New arc color. Should not be null.
     */
    public void setColor(int color) {
        mColor = color;
    }
    
    /**
     * Returns start angle of the arc.
     * 
     * @return Start angle.
     */
    public float getStartAngle() {
        return mStartAngle;
    }
    
    /**
     * Sets new start angle for the arc.
     * 
     * @param angle New start angle.
     */
    public void setStartAngle(float angle) {
        mStartAngle = angle;
        this.invalidate();
    }
    
    /**
     * Returns arc length in degrees.
     * 
     * @return Arc length.
     */
    public float getSwipeAngle() {
        return mSwipeAngle;
    }
    
    /**
     * Sets new arc length.
     * 
     * @param length Arc length.
     */
    public void setSwipeAngle(float length) {
        mSwipeAngle = length;
        this.invalidate();
    }
    
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
      
        Paint paint = new Paint();
        paint.setColor(mColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        
        RectF bounds = new RectF(0, 0, this.getWidth(), this.getHeight());
        canvas.drawArc(bounds, mStartAngle, mSwipeAngle, true, paint);
    }
    
}
