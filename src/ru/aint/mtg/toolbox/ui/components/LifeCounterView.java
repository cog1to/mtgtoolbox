package ru.aint.mtg.toolbox.ui.components;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.LifeCounter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LifeCounterView extends RelativeLayout implements LifeCounter.OnChangeListener {

    private TextView mMainTextView;    
    private ImageView mUpperLeftButton;
    private ImageView mUpperRightButton;
    
    public LifeCounterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateView(context);
    }

    public LifeCounterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateView(context);
    }
    
    public LifeCounterView(Context context) {
        super(context);
        inflateView(context);
    }
    
    private void inflateView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.counter_view, null));
        
        mMainTextView = (TextView)this.findViewById(R.id.main_label);
        mUpperLeftButton = (ImageView)this.findViewById(R.id.counter_button_left);
        mUpperRightButton = (ImageView)this.findViewById(R.id.counter_button_right);
    }
    
    /*
     * LifeCounter.OnChangeListener
     */
    
    @Override
    public void onLifeTotalChanged(int lifeTotal) {
        mMainTextView.setText("" + lifeTotal);
    }

    @Override
    public void onPoisonCounterChanged(int poisonCounter) { }


    @Override
    public void onLifeTotalIncreased(int lifeTotal) {
        mMainTextView.setText("" + lifeTotal);        
    }


    @Override
    public void onLifeTotalDecreased(int lifeTotal) {
        mMainTextView.setText("" + lifeTotal);
    }
    
    /*
     * Getters
     */
    
    public ImageView getLeftButton() {
        return mUpperLeftButton;
    }
    
    public ImageView getRightButton() {
        return mUpperRightButton;
    }
}
