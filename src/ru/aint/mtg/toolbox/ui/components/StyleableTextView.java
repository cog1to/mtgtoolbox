package ru.aint.mtg.toolbox.ui.components;

import ru.aint.mtg.toolbox.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * TextView with custom font.
 */
public class StyleableTextView extends TextView {
    
    /**
     * Name of a fonts folder inside "assets" folder.
     */
    private static String sFontsDirName = "fonts";
    
    /**
     * Default extension for fonts file.
     */
    private static String sFontsDefaultExtension = "ttf";
    
    /**
     * Regular font name suffix.
     */
    private static String sFontStyleRegularSuffix = "Regular";
    
    /**
     * Bold font name suffix.
     */
    private static String sFontStyleBoldSuffix = "Bold";
    
    /**
     * Italic font name suffix.
     */
    private static String sFontStyleItalicSuffix = "Italic";
    
    /**
     * Bold italic font name suffix.
     */
    private static String sFontStyleBoldItalicSuffix = "BoldItalic";
    
    /**
     * Font name.
     */
    private String mFontName;
    
    /**
     * Simple constructor to use when creating a view from code.
     * 
     * @param context Application context.
     */
    public StyleableTextView(Context context) {
        super(context);
    }
    
    /**
     * Constructor that is called when inflating a view from XML.
     * 
     * @param context Application context.
     * @param attrs Attributes.
     */
    public StyleableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parseAttributes(context, attrs);
    }
    
    /**
     * Perform inflation from XML and apply a class-specific base style.
     * 
     * @param context Application context.
     * @param attrs Attributes.
     * @param defStyle Style ID.
     */
    public StyleableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.parseAttributes(context, attrs);
    }    
    
    /**
     * Applies custom attributes to the TextView.
     * 
     * @param context Application context.
     * @param attrs Attribute set.
     */
    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray styleAttrs = context.obtainStyledAttributes(attrs, R.styleable.StyleableTextView);
        
        // Get attributes inherited from TextView.
        int[] set = {
                android.R.attr.textStyle
        };
        TypedArray inheritedAttrs = context.obtainStyledAttributes(attrs, set);
        int style = inheritedAttrs.getInt(0, Typeface.NORMAL);
        inheritedAttrs.recycle();
        
        // Cycle through all provided attributes.
        for (int i = 0; i < styleAttrs.getIndexCount(); i++) {
            int attr = styleAttrs.getIndex(i);
            switch (attr) {
                case R.styleable.StyleableTextView_fontName:
                    // User has provided font name, try to load it.
                    try {
                        String fontName = styleAttrs.getString(attr);
                        if (fontName.length() > 0) {
                            mFontName = fontName;                            
                        }
                    } catch (Exception ex) {
                        Log.d("StyleableTextView", "Error while setting font asset: " + ex.getMessage());
                    }
                    break;
                default:
                    break;
            }
        }
        styleAttrs.recycle();
        
        applyFont(mFontName, style);
    }
    
    /**
     * Tries to create a typeface with the specified name & style.
     * 
     * @param name Font family name.
     * @param style Text style.
     * 
     * @return Typeface with the given name & style.
     */
    private Typeface createTypeface(String name, int style) {
        if (style == Typeface.BOLD) {
            return Typeface.createFromAsset(getContext().getAssets(), String.format("%s/%s%s.%s", sFontsDirName, name, sFontStyleBoldSuffix, sFontsDefaultExtension));
        } else if (style == Typeface.ITALIC) {
            return Typeface.createFromAsset(getContext().getAssets(), String.format("%s/%s%s.%s", sFontsDirName, name, sFontStyleItalicSuffix, sFontsDefaultExtension));
        }  else if (style == Typeface.BOLD_ITALIC) {
            return Typeface.createFromAsset(getContext().getAssets(), String.format("%s/%s%s.%s", sFontsDirName, name, sFontStyleBoldItalicSuffix, sFontsDefaultExtension));
        } else {
            return Typeface.createFromAsset(getContext().getAssets(), String.format("%s/%s%s.%s", sFontsDirName, name, sFontStyleRegularSuffix, sFontsDefaultExtension));
        }
    }
    
    /**
     * Returns current font name.
     * 
     * @return Current font name.
     */
    public String getFontName() {
        return mFontName;
    }
    
    /**
     * Sets new font name.
     * 
     * @param fontName New font name.
     */
    public void setFontName(String fontName) {
        if (fontName == null) {
            return;
        }
        
        int style = getTypeface().getStyle();
        if (applyFont(fontName, style)) {
            mFontName = fontName;
        }
    }
    
    /**
     * Tries to apply custom font with the given name & style.
     * 
     * @param name Font name.
     * @param style Font style. Should be one of the {@link android.graphics.Typeface} style constants.
     * 
     * @return <b>true</b> if font was applied successfully, <b>false</b> otherwise. 
     */
    private boolean applyFont(String name, int style) {
        boolean success = true;
        Typeface typeface = null;
        
        try {
            typeface = createTypeface(name, style);
        } catch (Exception e) {
            Log.d("MTG", "Error while creating typeface: " + e.getMessage());
        }
        
        if (typeface == null) {
            success = false;
            typeface = Typeface.defaultFromStyle(style);
        }        
        this.setTypeface(typeface);
        
        return success;
    }
}

