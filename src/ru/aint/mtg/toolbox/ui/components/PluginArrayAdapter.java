package ru.aint.mtg.toolbox.ui.components;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.plugins.BasePlugin;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PluginArrayAdapter extends ArrayAdapter<BasePlugin> {

    /**
     * Single list item layout ID.
     */
    private int mItemLayoutId;
    
    /**
     * Plugins array.
     */
    private BasePlugin[] mItems;
    
    /**
     * Application context.
     */
    private Context mContext;
    
    /**
     * Title typeface.
     */
    private Typeface mTitleTypeface = null;
    
    /**
     * Description typeface.
     */
    private Typeface mDescriptionTypeface = null;
    
    /**
     * Helper class for Plugin views.
     */
    public class PluginHolder {
        public TextView titleView;
        public TextView descriptionView;
        public ImageView arrowView;
    }
    
    /**
     * Basic constructor.
     * 
     * @param context Application context.
     * @param viewResourceId Resource ID for layout view.
     * @param objects Array of plugins to populate into list.
     */
    public PluginArrayAdapter(Context context, int viewResourceId, BasePlugin[] objects) {
        super(context, viewResourceId, objects);
        mItemLayoutId = viewResourceId;
        mContext = context;
        mItems = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PluginHolder holder = null;
     
        if(row == null) {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            row = inflater.inflate(mItemLayoutId, parent, false);
            
            holder = new PluginHolder();
            holder.titleView = (TextView)row.findViewById(R.id.title_text_view);
            holder.descriptionView = (TextView)row.findViewById(R.id.description_text_view);
            holder.arrowView = (ImageView)row.findViewById(R.id.accessory_arrow);
            
            row.setTag(holder);
        } else {
            holder = (PluginHolder)row.getTag();
        }
        
        BasePlugin plugin = mItems[position];
        holder.titleView.setTypeface(getTitleTypeface());
        holder.titleView.setText(plugin.getTitle());
        holder.descriptionView.setText(plugin.getDescription());
        holder.descriptionView.setTypeface(getDescriptionTypeface());
        
        return row;
    }
    
    /**
     * Returns typeface for title view.
     * 
     * @return Typeface for title view.
     */
    private Typeface getTitleTypeface() {
        if (mTitleTypeface == null) {
            try {
                mTitleTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/CenturyGothicBold.ttf");
            } catch (Exception ex) {
                Log.d("DEBUG", "error: " + ex.getMessage());
            }
        }
        return mTitleTypeface;
    }
    
    /**
     * Returns typeface for description view.
     * 
     * @return Typeface for description view.
     */
    private Typeface getDescriptionTypeface() {
        if (mDescriptionTypeface == null) {
            try {
                mDescriptionTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/CenturyGothicRegular.ttf");
            } catch (Exception ex) {
                Log.d("DEBUG", "error: " + ex.getMessage());
            }
        }
        return mDescriptionTypeface;
    }
}
