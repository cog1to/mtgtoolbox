package ru.aint.mtg.toolbox.ui.components.common;

/**
 * Common interface for all progress displays.
 */
public interface ProgressDisplay {
    
    /**
     * Sets title.
     * 
     * @param title Title string to display.
     */
    public void setTitle(String title);
    
    /**
     * Sets title string.
     * 
     * @param resource String's resource ID.
     */
    public void setTitle(int resource);
    
    /**
     * Sets subtitle.
     * 
     * @param subtitle Subtitle string to display.
     */
    public void setSubtitle(String subtitle);
    
    /**
     * Sets subtitle.
     * 
     * @param resource Subtitle string to display.
     */
    public void setSubtitle(int resource);
    
    /**
     * Sets progress. Value must be from 0.0 to 1.0. 
     * 
     * @param progress Progress value.
     */
    public void setProgress(float progress);
}
