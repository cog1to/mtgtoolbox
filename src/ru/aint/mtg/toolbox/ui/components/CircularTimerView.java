/**
 * 
 */
package ru.aint.mtg.toolbox.ui.components;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.ui.components.common.ProgressDisplay;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Circular timer view. 
 */
public class CircularTimerView extends RelativeLayout implements ProgressDisplay {

    /**
     * Represents maximum progress (100%) in degrees.
     */
    private static float sMaxProgressInDegrees = 360;
    
    /**
     * Main label view.
     */
    private TextView mLabelView;
    
    /**
     * Sublabel view.
     */
    private TextView mSublabelView;
    
    /**
     * Arc view.
     */
    private ArcView mArcView;

    /**
     * Basic constructor.
     * 
     * @param context Application context.
     */
    public CircularTimerView(Context context) {
        super(context);
        this.inflateView(context);
    }

    /**
     * Basic XML constructor.
     * 
     * @param context Application context.
     * @param attrs Widget's attributes.
     */
    public CircularTimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.inflateView(context);
    }

    /**
     * Basic XML constructor with style ID provided.
     * 
     * @param context Application context.
     * @param attrs Widget's attributes.
     * @param defStyle Style ID.
     */
    public CircularTimerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.inflateView(context);
    }

    /**
     * Inflates view with proper subviews.
     * 
     * @param context Application context.
     */
    private void inflateView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.timer_view, null));
        
        mLabelView = (TextView)this.findViewById(R.id.timer_label);
        mSublabelView = (TextView)this.findViewById(R.id.timer_sublabel);
        mArcView = (ArcView)this.findViewById(R.id.timer_arc);
    }
    
    /**
     * Returns label view.
     * 
     * @return Label view.
     */
    public TextView getLabelView() {
        return mLabelView;
    }
    
    /**
     * Returns sublabel view.
     * 
     * @return Sublabel view.
     */
    public TextView getSublabelView() {
        return mSublabelView;
    }
    
    /**
     * Returns arc view.
     * 
     * @return Arc view.
     */
    public ArcView getArcView() {
        return mArcView;
    }
    
    /**
     * Sets text for main label.
     * 
     * @param text Text to display in main label.
     */
    @Override
    public void setTitle(String text) {
        mSublabelView.setText(text);
    }
    
    /**
     * Sets text for main label.
     * 
     * @param text Text to display in main label.
     */
    @Override
    public void setTitle(int resource) {
        mSublabelView.setText(resource);
    }
    
    /**
     * Sets text for sublabel.
     * 
     * @param text Text to display in sublabel.
     */
    @Override
    public void setSubtitle(String text) {
        mLabelView.setText(text);
    }
    
    /**
     * Sets text for sublabel.
     * 
     * @param text Text to display in sublabel.
     */
    @Override
    public void setSubtitle(int resource) {
        mLabelView.setText(resource);
    }
    
    /**
     * Sets overall progress.
     * 
     * @param progress Progress.
     */
    public void setProgress(float progress) {
        mArcView.setSwipeAngle(progress * sMaxProgressInDegrees);
    }
    
    /**
     * Returns current progress value.
     * 
     * @return A value between 0.0 and 1.0.
     */
    public float getProgress() {
        return (mArcView.mSwipeAngle / sMaxProgressInDegrees);
    }
}
