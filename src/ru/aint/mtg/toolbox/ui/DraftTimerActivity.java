package ru.aint.mtg.toolbox.ui;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.DraftConfig;
import ru.aint.mtg.toolbox.data.DraftTimerState;
import ru.aint.mtg.toolbox.data.complextimer.ComplexTimer;
import ru.aint.mtg.toolbox.data.complextimer.ComplexTimerDataSource;
import ru.aint.mtg.toolbox.data.complextimer.ComplexTimerListener;
import ru.aint.mtg.toolbox.data.providers.DraftTimeConfigProvider;
import ru.aint.mtg.toolbox.preferences.DraftTimerPrefenreces;
import ru.aint.mtg.toolbox.ui.components.CircularTimerView;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DraftTimerActivity extends Activity implements ComplexTimerDataSource, ComplexTimerListener {

    /**
     * Number of milliseconds in second.
     */
    private static double MILLISECONDS_IN_SECOND = 1000.0;
    
    /**
     * Number of intervals in one pick (1: choose, 2: pass)
     */
    private static int INTERVALS_IN_PICK = 2;

    /**
     * Ready state. Before the play is hit.
     */
    public static int STATE_READY = 0;

    /**
     * Playing state. When timer is running.
     */
    public static int STATE_PLAYING = 1;

    /**
     * Pause state. When user hits pause and before he/she hits play again.
     */
    public static int STATE_PAUSE = 2;

    /**
     * Timer view.
     */
    private CircularTimerView mTimerView;

    /**
     * Play/pause button.
     */
    private RelativeLayout mPlayPauseButton;

    /**
     * Play/pause icon.
     */
    private ImageView mPlayPauseIcon;

    /**
     * Reset reminder text view.
     */
    private TextView mResetHelperView;

    /**
     * State of the timer.
     */
    private int mState;

    /**
     * Currently active draft configuration.
     */
    private DraftConfig mCurrentConfig;

    /**
     * Complex timer;
     */
    private ComplexTimer mTimer;
    
    /**
     * Sound pool.
     */
    private SoundPool mSoundPool;

    /**
     * High tone sound.
     */
    private int mHighToneSoundId;

    /**
     * Low tone sound.
     */
    private int mLowToneSoundId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.draft_timer_activity);        

        // Create sound pool and add necessary sound effects.
        if (mSoundPool == null) {
            mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
            mHighToneSoundId = mSoundPool.load(this, R.raw.tone_high, 1);
            mLowToneSoundId = mSoundPool.load(this, R.raw.tone_low, 1);
        }

        // Bind controls.
        mTimerView = (CircularTimerView)findViewById(R.id.timer_view);
        mResetHelperView = (TextView)findViewById(R.id.reset_helper_text);
        mPlayPauseIcon = (ImageView)findViewById(R.id.play_pause_icon);

        // Play/pause button setup.
        mPlayPauseButton = (RelativeLayout)findViewById(R.id.play_pause_button);        
        mPlayPauseButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mState == STATE_READY) {
                    start();
                } else if (mState == STATE_PAUSE) {
                    resume();
                } else {
                    pause();
                }
            }
        });
        mPlayPauseButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mState == STATE_PAUSE) {
                    reset();
                    return true;
                } else if (mState == STATE_PLAYING){
                    pause();
                    return true;
                }
                return false;
            }
        });
        
        // Set initial state.
        mCurrentConfig = new DraftConfig();
        mTimer = new ComplexTimer();
        mTimer.addListener(this);
        reset();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Deallocate sound pool to free memory.
        if (mSoundPool != null) {
            mSoundPool.release();
            mSoundPool = null;
        }

        pause();
        saveState();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Create sound pool and add necessary sound effects.
        if (mSoundPool == null) {
            mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
            mHighToneSoundId = mSoundPool.load(this, R.raw.tone_high, 1);
            mLowToneSoundId = mSoundPool.load(this, R.raw.tone_low, 1);
        }

        loadState();
    }   

    /**
     * Saves current state in preferences.
     */
    private void saveState() {
        DraftTimerState state = new DraftTimerState(mCurrentConfig.getID(), mTimer.getCurrentTask(), (float)mTimer.getCurrentTime() / (float)MILLISECONDS_IN_SECOND);
        DraftTimerPrefenreces.saveState(getApplicationContext(), state);
    }

    /**
     * Loads state from preferences.
     */
    private void loadState() {
        DraftTimerState state = DraftTimerPrefenreces.getState(getApplicationContext());

        if (state != null) {
            mCurrentConfig = new DraftTimeConfigProvider(getApplicationContext()).getConfig(state.getConfigId());
            mTimer.setDataSource(this);
            mTimer.setCurrentTime(state.getStep(), (long)Math.floor((float)state.getTime() * (float)MILLISECONDS_IN_SECOND));
            mState = ((state.getStep() > 0) || (state.getTime() > 0.0f)) ? STATE_PAUSE : STATE_READY ;
        }
        updateUiWithCurrentState();
    }

    /**
     * Pauses timer's execution.
     */
    private void pause() {
        if (mState != STATE_PAUSE) {
            mTimer.pause();
        }
    }
    
    /**
     * Puts timer to the initial state.
     */
    private void reset() {
        mTimer.reset();
        mState = STATE_READY;
        updateUiWithCurrentState();
    }
    
    /**
     * Starts timer.
     */
    private void start() {
        if (mCurrentConfig != null) {
            mTimer.setDataSource(this);
            mTimer.start();
        }
    }
    
    /**
     * Resumes timer's execution.
     */
    private void resume() {
        if (mCurrentConfig != null) {
            mTimer.resume();
        }
    }
    
    /**
     * Updates UI elements.
     */
    private void updateUiWithCurrentState() {
        if (mState == STATE_READY) {
            switchReminder(false);
            switchPlayPause(true);
            
            if (mCurrentConfig != null) {
                mTimerView.setTitle(String.format("Pick %02d", 1));
                mTimerView.setSubtitle(String.format("%.1f", mCurrentConfig.getPickTimings()[0] + mCurrentConfig.getPassTime()));
                mTimerView.setProgress(0.0f);
            } else {
                mTimerView.setTitle("");
                mTimerView.setSubtitle("");
                mTimerView.setProgress(0.0f);
            }
        } else if (mState == STATE_PAUSE) {
            if (mCurrentConfig != null) {
                if (mTimer.getCurrentInterval() < 1) {
                    mTimerView.setTitle(String.format("Pick %02d", mTimer.getCurrentTask() + 1));
                } else {
                    mTimerView.setTitle(String.format("Pass"));
                }
                mTimerView.setSubtitle(String.format("%.1f", (float)mTimer.getTimeLeft() / (float)MILLISECONDS_IN_SECOND));
                mTimerView.setProgress(mTimer.getProgress());
            } 
            switchReminder(true);
            switchPlayPause(true);
        } else {
            switchReminder(false);
            switchPlayPause(false);
        }
    }
    
    /**
     * Switches play/pause button visual state.
     * 
     * @param pause Pause state flag.
     */
    private void switchPlayPause(boolean pause) {
        if (pause) {
            mPlayPauseIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_play));
        } else {
            mPlayPauseIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_pause));
        }
    }
    
    /**
     * Swithes reset reminder visual state.
     * 
     * @param show Visibility flag.
     */
    private void switchReminder(boolean show) {
        if (show) {
            mResetHelperView.setVisibility(View.VISIBLE);
        } else {
            mResetHelperView.setVisibility(View.INVISIBLE);
        }
    }
    
    /**
     * Plays pick signal.
     */
    private void playPickSound() {
        try {
            AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
            if (audioManager != null) {
                if ((audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) > 0) && (mSoundPool != null)) {                    
                    mSoundPool.play(mLowToneSoundId, 100.0f, 100.0f, 0, 0, 1);
                } else {
                    Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
                    vibrator.vibrate(500);
                }
            }            
        } catch (Exception ex) {
            Log.d("MTGToolBox", "Error while playing audio: " + ex.getMessage());
        }
    }

    /**
     * Plays pass signal.
     */
    private void playPassSound() {
        try {
            AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
            if (audioManager != null) {
                if ((audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) > 0) && (mSoundPool != null)) {                    
                    mSoundPool.play(mHighToneSoundId, 100.0f, 100.0f, 0, 0, 1);
                } else {
                    Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
                    vibrator.vibrate(500);
                }
            }
        } catch (Exception ex) {
            Log.d("MTGToolBox", "Error while playing audio: " + ex.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.draft_timer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.draft_timer_menu_select_config:
                selectNewConfig();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Loads new config.
     */
    private void selectNewConfig() {
        Intent intent = new Intent(this, DraftTimeConfigListActivity.class);
        startActivityForResult(intent, DraftTimeConfigListActivity.DRAFT_TIME_CONFIG_LIST_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DraftTimeConfigListActivity.DRAFT_TIME_CONFIG_LIST_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                DraftConfig selectedConfig = (DraftConfig)data.getExtras().getSerializable(DraftTimeConfigListActivity.DRAFT_TIME_CONFIG_LIST_RESULT_KEY);
                if (selectedConfig != null) {
                    mCurrentConfig = selectedConfig;
                    reset();
                    saveState();
                }
            }
        }
    }
    
    /// Timer listener.

    @Override
    public void onTimerStart() {
        mState = STATE_PLAYING;
        updateUiWithCurrentState();
    }

    @Override
    public void onTaskStarted(int task) { }

    @Override
    public void onIntervalStarted(int task, int interval) {
        if (interval == 0) {
            mTimerView.setTitle(String.format("Pick %02d", task + 1));
        } else {
            mTimerView.setTitle(String.format("Pass"));
        }
    }

    @Override
    public void onIntervalEnded(int task, int interval) {
        if (interval == 0) {
            playPassSound();
        } else {
            playPickSound();
        }
    }

    @Override
    public void onTaskEnded(int task) { }

    @Override
    public void onTimerFinish() {
        reset();
    }

    @Override
    public void onTaskProgress(float progress, long timeLeft) {
        mTimerView.setProgress(progress);
        mTimerView.setSubtitle(String.format("%.1f", ((float)timeLeft / MILLISECONDS_IN_SECOND)));
    }

    @Override
    public void onTimerError(Exception exception) {
        reset();
    }
    
    @Override
    public void onTimerSwitched(boolean paused) {
        if (paused) {
            mState = STATE_PAUSE;
        } else {
            mState = STATE_PLAYING;
        }
        updateUiWithCurrentState();
    }
    
    /// Data source.

    @Override
    public int getNumberOfTasks() {
        if (mCurrentConfig != null) {
            return mCurrentConfig.getPickTimings().length;
        } else {
            return 0;
        }
    }

    @Override
    public int getNumberOfIntervalsInTask(int task) {
        return INTERVALS_IN_PICK;
    }

    @Override
    public long[] getIntervals(int task) {
        if (mCurrentConfig != null) {
            float[] picks = mCurrentConfig.getPickTimings();
            return new long[] { (long)Math.floor((double)picks[task] * MILLISECONDS_IN_SECOND), 
                                (long)Math.floor((double)mCurrentConfig.getPassTime() * MILLISECONDS_IN_SECOND) };
        } else {
            return new long[0];
        }
    }
}
