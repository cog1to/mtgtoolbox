package ru.aint.mtg.toolbox.data.plugins;

import android.content.Context;
import android.content.Intent;

/**
 * Simple 'start activity' plugin.
 */
public class ActivityPlugin extends BasePlugin {

    /**
     * Parameterized constructor.
     * 
     * @param title Title string.
     * @param desc Description string.
     * @param target Target object.
     */
    public ActivityPlugin(String title, String desc, Object target) {
        super(title, desc, target);
    }
    
    @Override
    public void execute(Context context) {
        Intent action = new Intent((String)mTarget);
        action.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |  Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(action);
    }

}
