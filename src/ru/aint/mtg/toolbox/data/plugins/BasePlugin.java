package ru.aint.mtg.toolbox.data.plugins;

import android.content.Context;

/**
 * Abstract plugin interface declaration.
 * Plugins are entry points to different activities.
 */
public abstract class BasePlugin {
    
    /**
     * Title string.
     */
    protected String mTitle;
    
    /**
     * Description string.
     */
    protected String mDescription;
    
    /**
     * Target object.
     */
    protected Object mTarget;
    
    /**
     * Default constructor.
     */
    public BasePlugin() {}
    
    /**
     * Parameterized constructor.
     * 
     * @param title Title string.
     * @param desc Description string.
     * @param target Target object.
     */
    public BasePlugin(String title, String desc, Object target) {
        mTitle = title;
        mDescription = desc;
        mTarget = target;
    }
    
    /**
     * Returns plugin's title.
     * 
     * @return Plugin's title string.
     */
    public String getTitle() {
        return mTitle;
    }
    
    /**
     * Sets new plugin title.
     * 
     * @param title New title string.
     */
    public void setTitle(String title) {
        mTitle = title;
    }
    
    /**
     * Returns plugin's description.
     * 
     * @return Plugin's description string.
     */
    public String getDescription() {
        return mDescription;
    }
    
    /**
     * Sets new plugin's description.
     * 
     * @param desc New description string.
     */
    public void setDescription(String desc) {
        mDescription = desc;
    }
    
    /**
     * Returns plugin's target.
     * 
     * @return Plugin's target object.
     */
    public Object getTarget() {
        return mTarget;
    }
    
    /**
     * Sets new plugin's target.
     * 
     * @param target New target object.
     */
    public void setTitle(Object target) {
        mTarget = target;
    }
    
    /**
     * Executes plugin's action.
     * 
     * @param context Current applciation context.
     */
    public abstract void execute(Context context);
}
