package ru.aint.mtg.toolbox.data.providers;

import java.util.ArrayList;

import android.content.Context;

import ru.aint.mtg.toolbox.R;
import ru.aint.mtg.toolbox.data.plugins.ActivityPlugin;
import ru.aint.mtg.toolbox.data.plugins.BasePlugin;

/**
 * Holds information about all available activities.
 */
public class HubActivitiesProvider {

    /**
     * Application context.
     */
    Context mContext;
    
    /**
     * List of all available activities.
     */
    private ArrayList<ActivityPlugin> mActivities;

    /**
     * Default constructor.
     */
    public HubActivitiesProvider(Context context) {
        mContext = context;
        mActivities = new ArrayList<ActivityPlugin>();
        populateList();
    }

    /**
     * Create a list of activities.
     */
    private void populateList() {
        String lifeCounterActivity = "ru.aint.mtg.toolbox.ui.LifeCounterActivity";
        String lifeCounterTitle = mContext.getResources().getString(R.string.life_counter_activity_title);
        String lifeCounterDescription = mContext.getResources().getString(R.string.life_counter_activity_subtitle);
        ActivityPlugin lifeCounterPlugin = new ActivityPlugin(lifeCounterTitle, lifeCounterDescription, lifeCounterActivity);
        mActivities.add(lifeCounterPlugin);
        
        String draftTimerActivity = "ru.aint.mtg.toolbox.ui.DraftTimerActivity";
        String draftTimerTitle = mContext.getResources().getString(R.string.draft_timer_activity_title);
        String draftTimerDescription = mContext.getResources().getString(R.string.draft_timer_activity_subtitle);
        ActivityPlugin draftTimerPlugin = new ActivityPlugin(draftTimerTitle, draftTimerDescription, draftTimerActivity);
        mActivities.add(draftTimerPlugin);
    }
    
    /**
     * Returns array of plugin items representing available activities.
     * 
     * @return Array of activity plugins.
     */
    public BasePlugin[] getActivities() {
        try {
            BasePlugin[] arr = new BasePlugin[mActivities.size()];
            mActivities.toArray(arr);
            return arr;
        } catch (Exception ex) {
            return null;
        }
    }
}
