package ru.aint.mtg.toolbox.data.providers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import ru.aint.mtg.toolbox.data.DraftConfig;
import ru.aint.mtg.toolbox.preferences.LocalPersistence;
import android.content.Context;

public class DraftTimeConfigProvider {

    /**
     * Name of a local folder to store draft configs. 
     */
    public static String CONFIGS_DIR_PATH = "draftconfigs";
    
    /**
     * Extension for draft config files.
     */
    public static String CONFIG_EXTENSION = ".draftconfig";
    
    /**
     * Application context.
     */
    Context mContext;
    
    /**
     * Public constructor.
     * 
     * @param context Application context.
     */
    public DraftTimeConfigProvider(Context context) {
        mContext = context;
    }
    
    /**
     * Get config matching given ID.
     * 
     * @param id ID string of the config.
     * 
     * @return DraftConfig with the given ID.
     */
    public DraftConfig getConfig(String id) {
        if (id.equals(DraftConfig.DEFAULT_TOURNAMENT_CONFIG_ID)) {
            return DraftConfig.TournamentConfig();
        } else if (id.equals(DraftConfig.CASUAL_DRAFT_CONFIG_ID)) {
            return DraftConfig.CasualDraftConfig();
        } else {
            return null;
        }
    }
    
    /**
     * Returns array of all available configs.
     * 
     * @return Array of config ids.
     */
    public String[] getConfigNamesList() {
        String[] files = getConfigDirFile().list(new FilenameFilter() {            
            @Override
            public boolean accept(File dir, String filename) {
                String extension = filename.split("\\.", 2)[1];
                if ((extension != null) && (extension.equals(".draftconfig"))) {
                    return true;
                }
                return false;
            }
        });
        
        String[] fullList = new String[files.length+2];
        fullList[files.length] = DraftConfig.DEFAULT_TOURNAMENT_CONFIG_ID;
        fullList[files.length+1] = DraftConfig.CASUAL_DRAFT_CONFIG_ID;
        return fullList;
    }
    
    /**
     * Returns list of all saved configs.
     * 
     * @throws IOException 
     */
    public DraftConfig[] getConfigs() throws IOException {
        File configDir = getConfigDirFile();
        String[] files = configDir.list(new FilenameFilter() {            
            @Override
            public boolean accept(File dir, String filename) {
                String extension = filename.split("\\.", 2)[1];
                if ((extension != null) && (extension.equals(".draftconfig"))) {
                    return true;
                }
                return false;
            }
        });
        
        DraftConfig[] configs = new DraftConfig[files.length+2];
        int idx = 0;
        for (String filename : files) {
            String fullPath = configDir.getPath() + filename;
            configs[idx++] = (DraftConfig)LocalPersistence.readObjectFromFile(mContext, fullPath);
        }
        configs[idx] = DraftConfig.TournamentConfig();
        configs[idx+1] = DraftConfig.CasualDraftConfig(); 
        
        return configs;
    }
    
    /**
     * Saves config in file.
     * 
     * @param config Config to save.
     * 
     * @throws IOException
     */
    public void saveConfig(DraftConfig config) throws IOException {
        File configDir = getConfigDirFile();
        String fullFileName = configDir.getPath() + config.getID() + "." + CONFIG_EXTENSION;
        LocalPersistence.writeObjectToFile(mContext, config, fullFileName);
    }
    
    /**
     * Returns default folder for storing configs.
     * 
     * @return Default folder File for storing configs.
     */
    private File getConfigDirFile() {
        return mContext.getDir(CONFIGS_DIR_PATH, Context.MODE_PRIVATE);
    }
}
