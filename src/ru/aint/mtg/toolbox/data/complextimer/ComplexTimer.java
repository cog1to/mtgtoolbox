package ru.aint.mtg.toolbox.data.complextimer;

import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Complex timer implementation. 
 */
public class ComplexTimer {

    /**
     * Update interval.
     */
    private static final int UPDATE_INTERVAL = 100;
    
    /**
     * Data source.
     */
    ComplexTimerDataSource mDataSource;
    
    /**
     * Event listeners.
     */
    ArrayList<ComplexTimerListener> mListeners = new ArrayList<ComplexTimerListener>();
    
    /// Internal state variables
    
    /**
     * Overall number of tasks.
     */
    private long mNumberOfTasks;
    
    /**
     * Current task number.
     */
    private int mCurrentTask;

    /**
     * Current task summarized time limit.
     */
    private long mCurrentTaskTimeLimit;
    
    /**
     * Time progress in current time. 
     */
    private long mCurrentTaskNowTime;
    
    /**
     * Intervals in current task.
     */
    private long[] mTaskIntervals;

    /**
     * Current interval time.
     */
    private int mCurrentInterval;
    
    /**
     * Time when next interval is reached.
     */
    private long mNextIntervalTime;
        
    /**
     * Stop flag.
     */
    private boolean mStopped;
    
    /**
     * Update handler created on the original thread.
     */
    private RunnableHandler mHandler;
    
    /**
     * Update thread.
     */
    private UpdateThread mUpdateThread;
    
    /// Methods
    
    /**
     * Sets data source.
     */
    public void setDataSource(ComplexTimerDataSource source) {
        mStopped = true;
        mDataSource = source;
        reset();
    }

    /**
     * Adds listener.
     * 
     * @param listener New listener.
     */
    public void addListener(ComplexTimerListener listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }
    
    /**
     * Removes listener.
     * 
     * @param listener Listener to remove.
     */
    public void removeListener(ComplexTimerListener listener) {
        if (mListeners.contains(listener)) {
            mListeners.remove(listener);
        }
    }
    
    /**
     * Default empty constructor.
     */
    public ComplexTimer() {
        mHandler = new RunnableHandler();
        reset();
    }
    
    /**
     * Constructor with data source.
     * 
     * @param source Data source to query.
     */
    public ComplexTimer(ComplexTimerDataSource source) {
        mDataSource = source;
        mHandler = new RunnableHandler();
        reset();
    }
    
    /**
     * Starts the timer.
     */
    public void start() {
        if (mDataSource == null) {
            throw new IllegalStateException(getClass().getSimpleName() + ": Data source was not set.");
        }
        reset();        

        // Notify listeners that we're starting.
        for (ComplexTimerListener listener : mListeners) {
            listener.onTimerStart();
        }
        
        resume();
    }
    
    /**
     * Pauses execution.
     */
    public void pause() {
        try {
            mStopped = true;
            mUpdateThread.interrupt();
            // Notify listeners that the timer state was switched.
            for (ComplexTimerListener listener : mListeners) {
                listener.onTimerSwitched(mStopped);
            }
        } catch (Exception e) {
            for (ComplexTimerListener listener : mListeners) {
                listener.onTimerError(e);
            }
        }
    }
    
    /**
     * Resumes timer;
     */
    public void resume() {
        try {
            mStopped = false;
            // Notify listeners that we go to next interval.
            for (ComplexTimerListener listener : mListeners) {
                listener.onTimerSwitched(mStopped);
            }
            mUpdateThread = new UpdateThread(UPDATE_INTERVAL, mHandler);
            mUpdateThread.setDaemon(true);
            mUpdateThread.start();
        } catch (Exception e) {
            for (ComplexTimerListener listener : mListeners) {
                listener.onTimerError(e);
            }
        }
    }
    
    /**
     * Resets timer.
     */
    public void reset() {
        try {
            mStopped = false;
            if ((mUpdateThread != null) && (mUpdateThread.isAlive())) {
                mUpdateThread.interrupt();
            }
        
            if (mDataSource != null) {
                mNumberOfTasks = mDataSource.getNumberOfTasks();
                mCurrentTask = 0;
                mCurrentInterval = 0;
                setCurrentTime(0, 0);
                
            } else {
                mNumberOfTasks = 0;
                mTaskIntervals = null;
                mCurrentTask = -1;
                mCurrentInterval = -1;
                mNextIntervalTime = 0;
            }
        } catch (Exception e) {
            for (ComplexTimerListener listener : mListeners) {
                listener.onTimerError(e);
            }
        }
    }
    
    /**
     * Returns current task number.
     * 
     * @return Current task number.
     */
    public int getCurrentTask() {
        return mCurrentTask;
    }
    
    /**
     * Sets current task number.
     * 
     * @param task Current task number.
     * 
     * @throws IllegalArgumentException
     */
    public void setCurrentTask(int task) {
        if (mDataSource == null) {
            throw new IllegalStateException("Data source not set.");
        }
        
        if ((task < 0) || (mDataSource.getNumberOfTasks() <= mCurrentTask)) {
            throw new IllegalArgumentException("Illegal task number.");
        }
        
        mCurrentTask = task;
        
        // Reset current task progress.
        mTaskIntervals = mDataSource.getIntervals(mCurrentTask);
        mCurrentInterval = 0;
        mNextIntervalTime = mTaskIntervals[0];
        mCurrentTaskTimeLimit = 0;
        mCurrentTaskNowTime = 0;
        for (long interval : mTaskIntervals) {
            mCurrentTaskTimeLimit += interval;
        }
    }
    
    /**
     * Returns current interval number.
     * 
     * @return Current interval number.
     */
    public int getCurrentInterval() {
        return mCurrentInterval;
    }
    
    /**
     * Sets current interval in current task.
     * 
     * @param interval Interval number.
     * 
     * @throws IllegalStateException
     * @throws IllegalArgumentException
     */
    public void setCurrentInterval(int interval) {
        if (mDataSource == null) {
            throw new IllegalStateException("Data source not set.");
        }
        
        if ((interval < 0) || (mDataSource.getIntervals(mCurrentTask).length <= interval)) {
            throw new IllegalArgumentException("Illegal interval number.");
        }
        
        mCurrentInterval = interval;
        mNextIntervalTime = 0;
        for (int i = 0; i < mCurrentInterval; i++) {
            mNextIntervalTime += mTaskIntervals[i];
            if (i > 0) {
                mCurrentTaskNowTime += mTaskIntervals[i-1];
            }
        }
    }
    
    /**
     * Sets current task & interval.
     * 
     * @param task Task number.
     * @param interval Interval number.
     */
    public void setCurrentInteval(int task, int interval) {
        setCurrentTask(task);
        setCurrentInterval(interval);
    }
    
    /**
     * Returns time in current task.
     * 
     * @return Time in current task.
     */
    public long getCurrentTime() {
        return mCurrentTaskNowTime;
    }
    
    /**
     * Sets current time.
     * 
     * @param time Current time.
     * 
     * @throws IllegalStateException
     * @throws IllegalArgumentException
     */
    public void setCurrentTime(long time) {
        if (mDataSource == null) {
            throw new IllegalStateException("Data source not set.");
        }

        if ((time < 0) || (time > mCurrentTaskTimeLimit)) {
            throw new IllegalArgumentException("Illegal time.");
        }
        
        mCurrentTaskNowTime = time;
        mNextIntervalTime = 0;
        for (int interval = 0; interval < mTaskIntervals.length; interval++) {
            mNextIntervalTime += mTaskIntervals[interval];
            if (time > mNextIntervalTime) {
                mCurrentInterval++;
            } else {
                break;
            }
        }
    }
    
    /**
     * Sets current progress.
     * 
     * @param task Task number.
     * @param time Time in current task.
     */
    public void setCurrentTime(int task, long time) {
        setCurrentTask(task);
        setCurrentTime(time);
    }
    
    /**
     * Returns current progress.
     * 
     * @return Current progress as a value from 0.0 to 1.0.
     */
    public float getProgress() {
       return (float)mCurrentTaskNowTime / (float)mCurrentTaskTimeLimit;
    }
    
    /**
     * Returns amount of time left in task.
     * 
     * @return Amount of time left in task.
     */
    public long getTimeLeft() {
        return Math.max(mCurrentTaskTimeLimit - mCurrentTaskNowTime, 0l);
    }
    
    /// Internal methods.

    /**
     * Proceed to the next available interval;
     */
    private boolean goToNextInterval(long lead) {
        // Notify listeners that interval is finished.
        if (mCurrentInterval >= 0) {
            for (ComplexTimerListener listener : mListeners) {
                listener.onIntervalEnded(mCurrentTask, mCurrentInterval);
            }
        }
        
        mCurrentInterval++;
        if ((mTaskIntervals == null) || (mCurrentInterval == mTaskIntervals.length)) {
            boolean result = goToNextTask(lead);
            if (!result) {
                // We're finished here.
                return false;
            }
        } else {
            mNextIntervalTime += mTaskIntervals[mCurrentInterval];
        }
        
        // Notify listeners that we go to next interval.
        for (ComplexTimerListener listener : mListeners) {
            listener.onIntervalStarted(mCurrentTask, mCurrentInterval);
        }
        
        return true;
    }
    
    /**
     * Proceed to the next available task.
     * 
     * @returns <b>true</b> if timer is still going, <b>false</b> otherwise.
     */
    private boolean goToNextTask(long lead) {
        // Notify listeners that task is finished.
        if (mCurrentTask >= 0) {
            for (ComplexTimerListener listener : mListeners) {
                listener.onTaskEnded(mCurrentTask);            
            }
        }
        
        mCurrentTask++;
        // If there are no tasks, finish.
        if (mCurrentTask >= mNumberOfTasks) {
            return false;
        }
        
        // Reset current task progress.
        setCurrentTask(mCurrentTask);
        mCurrentTaskNowTime = lead;

        // Notify listeners that we go to next task.
        for (ComplexTimerListener listener : mListeners) {
            listener.onTaskStarted(mCurrentTask);
        }
        
        return true;
    }
    
    /**
     * Timer runnable;
     */
    private class UpdateThread extends Thread {
        
        /**
         * Update interval;
         */
        private long mUpdateInterval;
        
        /**
         * Last known timestamp.
         */
        private long mLastTime;
        
        /**
         * Current timepstamp.
         */
        private long mCurrentTime;
        
        /**
         * Listener.
         */
        private RunnableHandler mHandler;
        
        /**
         * Main constructor.
         * 
         * @param interval Update interval;
         */
        public UpdateThread(long interval, RunnableHandler handler) {
            mUpdateInterval = interval;
            mHandler = handler;
        }
        
        @Override
        public void run() {
            try {
                mCurrentTime = System.currentTimeMillis();
                while (true) {
                    sleep(mUpdateInterval);
                    mLastTime = mCurrentTime;
                    mCurrentTime = System.currentTimeMillis();
                    
                    long timePassed = mCurrentTime - mLastTime;
                    if ((mHandler != null) && (!mStopped)) {
                        sendMessage(Long.valueOf(timePassed), RunnableHandler.HANDLER_CAUSE_UPDATE);
                    }
                }
            } catch (Exception e) {
                if (e.getClass() != InterruptedException.class) {
                    if (mHandler != null) {
                        sendMessage(e, RunnableHandler.HANDLER_CAUSE_ERROR);
                    }
                }
            }
        };
        
        /**
         * Helper to pass updates to the handler.
         * 
         * @param obj Argument.
         * @param reason Reason ID.
         */
        private void sendMessage(Object obj, int reason) {
            if (mHandler != null) {
                Message msg = Message.obtain(mHandler, reason);
                msg.obj = obj;
                mHandler.sendMessage(msg);
            }
        }
    }
    
    /**
     * Runnable listener timer. 
     */
    private class RunnableHandler extends Handler {

        /**
         * Indicates normal update.
         */
        public static final int HANDLER_CAUSE_UPDATE = 0x00;
        
        /**
         * Indicates error case.
         */
        public static final int HANDLER_CAUSE_ERROR = 0x01;
        
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == HANDLER_CAUSE_UPDATE) {
                Long timePassed = (Long)msg.obj;
                if (!mStopped) {
                    mCurrentTaskNowTime += timePassed;
                    // Notify listeners about progress.
                    for (ComplexTimerListener listener : mListeners) {
                        // We don't use our public functions here for performance sake.
                        listener.onTaskProgress((float)mCurrentTaskNowTime / (float)mCurrentTaskTimeLimit, Math.max(mCurrentTaskTimeLimit - mCurrentTaskNowTime, 0l));
                    }
                
                    if (mCurrentTaskNowTime >= mNextIntervalTime) {
                        boolean result = goToNextInterval(mCurrentTaskNowTime - mNextIntervalTime);
                        Log.d("MTG", "current interval: " + mCurrentInterval + ", current interval time: " + mNextIntervalTime);
                        if (!result) {
                            mStopped = true;
                            // Notify listeners that we're finished.
                            for (ComplexTimerListener listener : mListeners) {
                                listener.onTaskProgress(1.0f, 0l);
                                listener.onTimerFinish();
                            }
                        }
                    }
                }
            } else if (msg.what == HANDLER_CAUSE_ERROR) {
                this.onError((Exception)msg.obj);
            }
            super.handleMessage(msg);
        }
        
        /**
         * Called on raised exception.
         * 
         * @param exception Exception that was raised.
         */
        private void onError(Exception exception) {
            // Notify listeners that we're got an error.
            for (ComplexTimerListener listener : mListeners) {
                mStopped = true;
                listener.onTimerError(exception);
            }
        }
    }
}
