package ru.aint.mtg.toolbox.data.complextimer;

/**
 * Complex timer listener. 
 */
public interface ComplexTimerListener {

    /**
     * Called when execution of tasks has started.
     */
    public void onTimerStart();
    
    /**
     * Called when task is started.
     * 
     * @param task Task number.
     */
    public void onTaskStarted(int task);
    
    /**
     * Called when particular interval in task is reached.
     * 
     * @param task Task number.
     * @param interval Interval number.
     */
    public void onIntervalStarted(int task, int interval);
    
    /**
     * Called when end of given interval is reached.
     * 
     * @param task Task number.
     * @param interval Interval number.
     */
    public void onIntervalEnded(int task, int interval);
    
    /**
     * Called when end of given task is reached.
     * 
     * @param task Task number.
     */
    public void onTaskEnded(int task);
    
    /**
     * Called when Execution of tasks has finished.
     */
    public void onTimerFinish();
    
    /**
     * Called to update task progress.
     */
    public void onTaskProgress(float progress, long timePassed);
    
    /**
     * Called when timer encounters an error while executing.
     */
    public void onTimerError(Exception exception);
    
    /**
     * Called when timer gets paused/resumed.
     */
    public void onTimerSwitched(boolean paused);
}
