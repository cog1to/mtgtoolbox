package ru.aint.mtg.toolbox.data.complextimer;

/**
 * Complex timer data source. 
 */
public interface ComplexTimerDataSource {
    
    /**
     * Returns number of timed tasks to go through.
     * 
     * @return Overall number of timed tasks to go through.
     */
    public int getNumberOfTasks();
    
    /**
     * Returns number of intervals in one given task.
     * 
     * @param task Task number.
     * 
     * @return Number of intervals in one task.
     */
    public int getNumberOfIntervalsInTask(int task);
    
    /**
     * Returns all intervals in one given task.
     * 
     * @param task Task number.
     * 
     * @return Array of time intervals for given task.
     */
    public long[] getIntervals(int task);
}
