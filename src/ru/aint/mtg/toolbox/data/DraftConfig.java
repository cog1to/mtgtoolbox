package ru.aint.mtg.toolbox.data;

import java.io.Serializable;

public class DraftConfig implements Serializable {

    /**
     * Serializable ID.
     */
    private static final long serialVersionUID = 1276849629279107945L;

    /**
     * Default tournament config id.
     */
    public static String DEFAULT_TOURNAMENT_CONFIG_ID = "ru.aint.mtg.toolbox.draft.tournament.default";
    
    /**
     * Casual draft config id.
     */
    public static String CASUAL_DRAFT_CONFIG_ID = "ru.aint.mtg.toolbox.draft.casual";
    
    /**
     * ID string.
     */
    private String mId;
    
    /**
     * Config name.
     */
    private String mName;
    
    /**
     * Config description.
     */
    private String mDescription;
    
    /**
     * Time devoted to pass cards to another player.
     */
    private float mPassTime;
    
    /**
     * Review time.
     */
    private float mReviewTime;
    
    /**
     * Time devoted to get a pick.
     */
    private float[] mPickTimings;
    
    /**
     * Custom constructor with pick timings & pass time.
     * 
     * @param id Identificator string. Should be unique.
     * @param pickTimings Pick timings array. Must be at least 13 elements long.
     * @param passTime Pass time. Must be more than zero.
     * @param reviewTime Pool review time. Must be more than zero.
     */
    public DraftConfig(String id, float[] pickTimings, float passTime, float reviewTime) {
        this.setID(id);
        this.setPassTime(passTime);
        this.setReviewTime(reviewTime);
        this.setPickTimings(pickTimings);
    }
    
    /**
     * Custom constructor with pick timings & pass time.
     * 
     * @param id Identificator string. Should be unique.
     * @param pickTimings Pick timings array. Must be at least 13 elements long.
     * @param passTime Pass time. Must be more than zero.
     * @param reviewTime Pool review time. Must be more than zero.
     * @param name Displayed name for the config.
     * @param description Displayed description for the config.
     */
    public DraftConfig(String id, float[] pickTimings, float passTime, float reviewTime, String name, String description) {
        this.setID(id);
        this.setPassTime(passTime);
        this.setReviewTime(reviewTime);
        this.setPickTimings(pickTimings);
        this.setName(name);
        this.setDescription(description);
    }
    
    /**
     * Constructor that initializes configuration with default draft timings.
     */
    public DraftConfig() {
        mId = DEFAULT_TOURNAMENT_CONFIG_ID;
        mName = "Tournament Draft";
        mDescription = "Tournament legal configuration";
        mPassTime = 5.0f;
        mReviewTime = 30.0f;
        mPickTimings = new float[] {40.0f, 40.0f, 
                                    35.0f, 30.0f, 
                                    25.0f, 25.0f, 
                                    20.0f, 20.0f, 
                                    15.0f, 10.0f,
                                    10.0f, 5.0f,
                                    5.0f};
    }
    
    /**
     * Static constructor for tournament configuration.
     * 
     * @return Draft timer config for tournament draft.
     */
    public static DraftConfig TournamentConfig() {
        return new DraftConfig();
    }
    
    /**
     * Static constructor for casual draft configuration.
     * 
     * @return Draft timer config for casual play.
     */
    public static DraftConfig CasualDraftConfig() {
        float[] timings = new float[] {50.0f, 50.0f,
                           45.0f, 40.0f,
                           35.0f, 35.0f,
                           30.0f, 25.0f,
                           20.0f, 15.0f,
                           10.0f, 5.0f,
                           5.0f};
        return new DraftConfig(CASUAL_DRAFT_CONFIG_ID, timings, 5.0f, 60.0f, "Casual draft config", "Configuration for casual draft plays");
    }
    
    /**
     * Returns ID string.
     * 
     * @return ID string.
     */
    public String getID() {
        return mId;
    }
    
    /**
     * Sets new ID string.
     * 
     * @param id New ID string.
     */
    public void setID(String id) {
        if (id == null) {
            throw new NullPointerException("ID string cannot be null");
        }
        
        mId = id;
    }
    
    /**
     * Returns value for pass time.
     * 
     * @return Pass time.
     */
    public float getPassTime() {
        return mPassTime;
    }
    
    /**
     * Sets the new pass time value.
     * 
     * @param passTime Pass time.
     */
    public void setPassTime(float passTime) {
        if (passTime < 0) {
            throw new IllegalArgumentException("Pass time should not be less than zero");
        }
        
        mPassTime = passTime;
    }
    
    /**
     * Returns pool review time;
     * 
     * @return Review time value.
     */
    public float getReviewTime() {
        return mReviewTime;
    }
    
    /**
     * Sets new value for pool review time.
     * 
     * @param reviewTime Time for review pool between packs.
     */
    public void setReviewTime(float reviewTime) {
        if (reviewTime < 0) {
            throw new IllegalArgumentException("Review time should not be less than zero");
        }
        
        mReviewTime = reviewTime;
    }
    
    /**
     * Returns array of timings for picks.
     * 
     * @return Picks timing array.
     */
    public float[] getPickTimings() {
        return mPickTimings;
    }
    
    /**
     * Sets new time values for picks.
     * 
     * @param pickTimings Array of pick timing values. Each value must be in bounds of [0.0; 90.0]
     */
    public void setPickTimings(float[] pickTimings) {
        if (pickTimings.length < 13) {
            throw new IllegalArgumentException("Timings array should not have less than 13 elements");
        }
        
        for (int i = 0; i < pickTimings.length; i++) {
            float timing = pickTimings[i];
            if ((timing < 0.0f) || (timing > 90.0f)) {
                throw new IllegalArgumentException("Pick Timing [" + i + "] has value beyond the allowed interval of [0.0; 90.0]");
            }
        }
                
        mPickTimings = pickTimings;
    }
    
    /**
     * Returns timing for the pick with the given index.
     * 
     * @param index Index of a pick.
     */
    public float getTimingForPick(int index) {
        if ((index < 0) || (index > (mPickTimings.length-1))) {
            throw new IllegalArgumentException("Invalid index value: " + index);
        }
        
        return mPickTimings[index];
    }
    
    /**
     * Returns name of the config.
     * 
     * @return Name string.
     */
    public String getName() {
        return mName;
    }
    
    /**
     * Sets new name for config.
     * 
     * @param name New name string.
     */
    public void setName(String name) {
        mName = name;
    }
    
    /**
     * Returns description string for config.
     * 
     * @return Description string.
     */
    public String getDescription() {
        return mDescription;
    }
    
    /**
     * Sets description for config.
     * 
     * @param description New description string.
     */
    public void setDescription(String description) {
        mDescription = description;
    }
}
