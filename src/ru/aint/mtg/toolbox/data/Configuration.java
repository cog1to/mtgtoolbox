package ru.aint.mtg.toolbox.data;

/**
 * Various default parameters holder.
 */
public class Configuration {
    /**
     * Default start life count.
     */
    public static int DEFAULT_START_LIFE_COUNT = 20;
}
