package ru.aint.mtg.toolbox.data;

import java.util.ArrayList;

/**
 * Class that holds basic info about player's life.
 */
public class LifeCounter {
    
    /**
     * On change listener.
     */
    public interface OnChangeListener {
        /**
         * Called when life total has been changed.
         * 
         * @param lifeTotal New life total value.
         */
        public void onLifeTotalChanged(int lifeTotal);
        
        /**
         * Called when poison counter has been changed.
         * 
         * @param poisonCounter New poison counter value.
         */
        public void onPoisonCounterChanged(int poisonCounter);
        
        /**
         * Called when life total is increased.
         * 
         * @param lifeTotal New life total value.
         */
        public void onLifeTotalIncreased(int lifeTotal);
        
        /**
         * Called when life total is decreased.
         * 
         * @param lifeTotal New life total value.
         */
        public void onLifeTotalDecreased(int lifeTotal);
    }
    
    /**
     * List of change listeners.
     */
    private ArrayList<OnChangeListener> mListeners;
    
    /**
     * Player's name.
     */
    private String mPlayerName = null;
    
    /**
     * Player's life total.
     */
    private int mLifeTotal = 20;
    
    /**
     * Player's poison counter.
     */
    private int mPoisonCounter = 0;
    
    /**
     * Empty constructor.
     */
    public LifeCounter() { 
        mListeners = new ArrayList<OnChangeListener>();
    }
    
    /**
     * Simple constructor with life total.
     * 
     * @param lifeTotal Initial life total.
     * 
     * @throws IllegalAccessException 
     */
    public LifeCounter(int lifeTotal) throws IllegalAccessException {
        this();
        setLifeTotal(lifeTotal);
    }
    
    /**
     * Simple constructor with life total and poison counter.
     * 
     * @param lifeTotal Initial life total.
     * @param poisonCounter Initial poison counter.
     * 
     * @throws IllegalAccessException 
     */
    public LifeCounter(int lifeTotal, int poisonCounter) throws IllegalAccessException {
        this(lifeTotal);
        setPoisonCounter(poisonCounter);
    }
 
    /**
     * Full constructor.
     * 
     * @param name Player's name.
     * @param lifeTotal Initial life total.
     * @param poisonCounter Initial poison counter.
     * 
     * @throws IllegalAccessException 
     */
    public LifeCounter(String name, int lifeTotal, int poisonCounter) throws IllegalAccessException {
        this(lifeTotal, poisonCounter);
        setName(name);
    }
    
    /**
     * Returns player's name.
     * 
     * @return Player's name.
     */
    public String getName() {
        return mPlayerName;
    }
    
    /**
     * Sets player's name.
     * 
     * @param name New name.
     */
    public void setName(String name) {
        mPlayerName = name;
    }
    
    /**
     * Returns life total.
     * 
     * @return Life total number.
     */
    public int getLifeTotal() {
        return mLifeTotal;
    }
    
    /**
     * Sets life total.
     * 
     * @param lifeTotal New life total. Must be more or equal to zero.
     * 
     * @throws IllegalAccessException 
     */
    public void setLifeTotal(int lifeTotal) throws IllegalAccessException {
        if (lifeTotal < 0) {
            throw new IllegalAccessException("Life total cannot be less that 0.");
        }
        
        mLifeTotal = lifeTotal;
        updateLifeTotal(lifeTotal, lifeTotal);
    }
    
    /**
     * Returns poison counter.
     * 
     * @return Poison counter value.
     */
    public int getPoisonCounter() {
        return mPoisonCounter;
    }
    
    /**
     * Sets poison counter.
     * 
     * @param counter New poison counter value.
     * 
     * @throws IllegalAccessException
     */
    public void setPoisonCounter(int counter) throws IllegalAccessException {
        if ((counter < 0) || (counter > 10)) {
            throw new IllegalAccessException("Poison counter must be a number between 0 and 10.");
        }
        
        mPoisonCounter = counter;
        updatePoisonCounter();
    }
    
    /**
     * Adds one poison counter.
     */
    public void increasePoisonCounter() {
        mPoisonCounter = Math.min(10, mPoisonCounter + 1);
        updatePoisonCounter();
    }
    
    /**
     * Removes one poison counter.
     */
    public void decreasePoisonCounter() {
        mPoisonCounter = Math.max(0, mPoisonCounter -1);
        updatePoisonCounter();
    }
    
    /**
     * Adds amount of poison counters.
     * 
     * @param value Value to add.
     */
    public void addPoisonCounter(int value) {
        mPoisonCounter = Math.min(10, mPoisonCounter + value);
        updatePoisonCounter();
    }
    
    /**
     * Removes amount of poison counters.
     * 
     * @param value Value to subtract.
     */
    public void substractPoisonCounter(int value) {
        mPoisonCounter = Math.max(0, mPoisonCounter - value);
        updatePoisonCounter();
    }
    
    /**
     * Adds one point to the life total.
     */
    public void increaseLifeTotal() {
        int oldValue = mLifeTotal; 
        mLifeTotal++;        
        
        updateLifeTotal(oldValue, mLifeTotal);
    }
    
    /**
     * Adds the <b>value</b> points of life.
     * 
     * @param value Number of life points to add.
     */
    public void addLifeTotal(int value) throws IllegalAccessException {
        int oldLifeTotal = mLifeTotal;
        mLifeTotal = Math.max(mLifeTotal + value, 0);
        updateLifeTotal(oldLifeTotal, mLifeTotal);
    }
    
    /**
     * Removes one life point.
     */
    public void decreaseLifeTotal() {
        int oldValue = mLifeTotal;
        mLifeTotal = Math.max(mLifeTotal - 1, 0);
        updateLifeTotal(oldValue, mLifeTotal);
    }
    
    /**
     * Removes <b>value</b> life points.
     * 
     * @param value Number of life total to remove.
     */
    public void subtractLifeTotal(int value) {
        int oldValue = mLifeTotal;
        mLifeTotal = Math.max(mLifeTotal - value, 0);
        updateLifeTotal(oldValue, mLifeTotal);
    }
    
    /**
     * Adds new change listener.
     * 
     * @param listener OnChangeListner object to register as a listener.
     */
    public void addOnChangeListener(OnChangeListener listener) {
        if (mListeners.indexOf(listener) == -1) {
            mListeners.add(listener);
            listener.onLifeTotalChanged(mLifeTotal);
            listener.onPoisonCounterChanged(mPoisonCounter);
        }
    }
    
    /**
     * Removes listener from list of on change listeners.
     * 
     * @param listener OnChangeListener object to exclude from listeners list.
     */
    public void removeOnChangeListener(OnChangeListener listener) {
        if (mListeners.indexOf(listener) != -1) {
            mListeners.remove(listener);
        }
    }
    
    /**
     * Propagates life total to the listeners.
     */
    protected void updateLifeTotal(int oldValue, int newValue) {
        if (mListeners != null) {
            for (OnChangeListener listener : mListeners) {
                if (newValue < oldValue) {
                    listener.onLifeTotalDecreased(newValue);
                } else if (newValue > oldValue) {
                    listener.onLifeTotalIncreased(newValue);
                } else {
                    listener.onLifeTotalChanged(newValue);
                }
                
            }
        }        
    }

    /**
     * Propagates poison counters number to the listeners.
     */
    protected void updatePoisonCounter() {
        if (mListeners != null) {
            for (OnChangeListener listener : mListeners) {
                listener.onPoisonCounterChanged(mPoisonCounter);
            }
        }
    }
}
