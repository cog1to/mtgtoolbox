package ru.aint.mtg.toolbox.data;

/**
 * Contains draft timer state snapshot.
 */
public class DraftTimerState {

    /**
     * Config ID.
     */
    private String mConfigId;
    
    /**
     * Step number; 
     */
    private int mStep;
    
    /**
     * Time in current section.
     */
    private float mTime;
    
    /**
     * Default constructor.
     */
    public DraftTimerState() { }
    
    /**
     * Parametrized constructor.
     * 
     * @param config Config ID.
     * @param step Step number.
     * @param time Step progress time.
     */
    public DraftTimerState(String config, int step, float time) {
        setConfigId(config);
        setStep(step);
        setTime(time);
    }
    
    /**
     * Returns Config ID.
     * 
     * @returns Config ID string.
     */
    public String getConfigId() {
        return mConfigId;
    }
    
    /**
     * Sets config ID.
     * 
     * @param Config ID.
     */
    public void setConfigId(String configId) {
        mConfigId = configId;
    }
    
    /**
     * Returns step number.
     * 
     * @return Step number.
     */
    public int getStep() {
        return mStep;
    }
    
    /**
     * Sets step number.
     * 
     * @param step Step number.
     */
    public void setStep(int step) {
        mStep = step;
    }
    
    /**
     * Returns time in section.
     * 
     * @return Time in section.
     */
    public float getTime() {
        return mTime;
    }
    
    /**
     * Sets time in section.
     * 
     * @param time Time in section.
     */
    public void setTime(float time) {
        mTime = time;
    }
}
