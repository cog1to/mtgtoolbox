package ru.aint.mtg.toolbox.preferences;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.util.Log;

/**
 * Serializes/deserializes an object to/from a private local file.
 */
public class LocalPersistence {

    /**
     * Serializes an object to a private local file.
     * 
     * @param context Application context.
     * @param object Object to serialize.
     * @param filename Filename to save to object to.
     *
     * @throws IOException 
     */
    public static void writeObjectToFile(Context context, Object object, String filename) throws IOException {

        ObjectOutputStream objectOut = null;
        try {
            FileOutputStream fileOut = context.openFileOutput(filename, Context.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
            fileOut.getFD().sync();
        } catch (IOException ex) {           
            Log.d("MTGToolBox", "Error saving object to a file: " + ex.getMessage());
        } finally {
            if (objectOut != null) {
                objectOut.close();              
            }
        }
    }

    /**
     * Deserializes an object from a private local file.
     * 
     * @param context Application context.
     * @param filename Filename to load to object from.
     * 
     * @return Deserialized object.
     * 
     * @throws IOException 
     */
    public static Object readObjectFromFile(Context context, String filename) throws IOException {
        ObjectInputStream objectIn = null;
        Object object = null;

        try {
            FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();
        } catch (FileNotFoundException ex) {
            // Do nothing
        } catch (IOException ex) {
            Log.d("MTGToolBox", "Error reading object from a file: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Log.d("MTGToolBox", "Error reading object from a file: " + ex.getMessage());
        } finally {
            if (objectIn != null) {
                objectIn.close();
            }
        }

        return object;
    }
}
