package ru.aint.mtg.toolbox.preferences;

import ru.aint.mtg.toolbox.data.DraftTimerState;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Draft timer preferences. 
 */
public class DraftTimerPrefenreces {
    
    /**
     * Filename for draft timer preferences.
     */
    private static String DRAFT_TIMER_PREFERENCES_FILE = "ru.aint.mtg.toolbox.draft_timer.prefs";
        
    /**
     * Draft timer's last active step index preference key.
     */
    private static String DRAFT_TIMER_CURRENT_STEP_PREF_KEY = "draft_timer.step";
    
    /**
     * Draft timer's timer left value key.
     */
    private static String DRAFT_TIMER_TIME_PREF_KEY = "draft_timer.time";
    
    /**
     * Draft timer's active config ID key.
     */
    private static String DRAT_TIMER_ACTIVE_CONFIG_PREF_KEY = "draft_timer.active_config_id";
        
    /**
     * Saves timer state.
     * 
     * @param context Application context.
     * @param state State.
     * @param configId Config's ID.
     * @param step Active section.
     * @param time Active section's time.
     */
    public static void saveState(Context context, String configId, int step, int time) {
        SharedPreferences preferences = context.getSharedPreferences(DRAFT_TIMER_PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        
        editor.putString(DRAT_TIMER_ACTIVE_CONFIG_PREF_KEY, configId);
        editor.putInt(DRAFT_TIMER_CURRENT_STEP_PREF_KEY, step);
        editor.putFloat(DRAFT_TIMER_TIME_PREF_KEY, time);
        editor.commit();
    }
    
    /**
     * Saves timer state.
     * 
     * @param state State object.
     */
    public static void saveState(Context context, DraftTimerState state) {
        SharedPreferences preferences = context.getSharedPreferences(DRAFT_TIMER_PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        
        editor.putString(DRAT_TIMER_ACTIVE_CONFIG_PREF_KEY, state.getConfigId());
        editor.putInt(DRAFT_TIMER_CURRENT_STEP_PREF_KEY, state.getStep());
        editor.putFloat(DRAFT_TIMER_TIME_PREF_KEY, state.getTime());
        editor.commit();
    }
    
    /**
     * Returns saved timer state.
     * 
     * @return
     */
    public static DraftTimerState getState(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(DRAFT_TIMER_PREFERENCES_FILE, Context.MODE_PRIVATE);
        
        if (preferences.contains(DRAT_TIMER_ACTIVE_CONFIG_PREF_KEY)) {
            
            DraftTimerState state = new DraftTimerState();
            state.setConfigId(preferences.getString(DRAT_TIMER_ACTIVE_CONFIG_PREF_KEY, null));
            state.setStep(preferences.getInt(DRAFT_TIMER_CURRENT_STEP_PREF_KEY, 0));
            state.setTime(preferences.getFloat(DRAFT_TIMER_TIME_PREF_KEY, 0.0f));
        
            return state;
        }
        
        return null;
    }
}