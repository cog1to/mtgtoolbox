package ru.aint.mtg.toolbox.preferences;

import java.io.IOException;
import java.io.Serializable;

import ru.aint.mtg.toolbox.data.LifeCounter;
import android.content.Context;
import android.util.Log;

/**
 * Preferences handler. 
 */
public class Preferences {
    
    /**
     * Filename for draft timer preferences.
     */
    public static String LIFE_COUNTER_PREFERENCES_FILE = "ru.aint.mtg.toolbox.life_counter.prefs";
    
    /**
     * Life counter preferences.
     */
    public static class LifeCounterPreferences {
        
        /**
         * Internal class to store life counters.
         */
        private static class LifeCounterContainer implements Serializable {
            /**
             * Serial version ID.
             */
            private static final long serialVersionUID = 6604575410126150603L;

            /**
             * Player's name.
             */
            public String playerName = null;
            
            /**
             * Player's life total.
             */
            public int lifeTotal = 20;
            
            /**
             * Player's poison counter.
             */
            public int poisonCounter = 0;
            
            /**
             * Parameterized constructor.
             */
            public LifeCounterContainer(String playerName, int lifeTotal, int poisonCounter) {
                this.playerName = playerName;
                this.lifeTotal = lifeTotal;
                this.poisonCounter = poisonCounter;
            }
        }
        
        /**
         * Returns life counters' state from the last active session.
         * 
         * @param context Application context.
         * 
         * @return Array of life counters saved from the previous state.
         * 
         * @throws IOException 
         */
        public static LifeCounter[] getLastSessionCounters(Context context) throws IOException {
            LifeCounterContainer[] containers = (LifeCounterContainer[]) LocalPersistence.readObjectFromFile(context, LIFE_COUNTER_PREFERENCES_FILE);
            
            if ((containers != null) && (containers.length > 0)) {
                LifeCounter[] counters = new LifeCounter[containers.length];
                for (int idx = 0; idx < containers.length; idx++) {
                    try {
                        counters[idx] = new LifeCounter(containers[idx].playerName, containers[idx].lifeTotal, containers[idx].poisonCounter);
                    } catch (IllegalAccessException e) {
                        Log.d("MTGToolbox", "Error while creating life counter from persistence file.");
                        e.printStackTrace();
                    }
                }
                return counters;
            }
            
            return null;
        }
        
        /**
         * Saves life counters state.
         * 
         * @param context Application context.
         * @param counters Life counters to save.
         * 
         * @throws IOException 
         */
        public static void setLastSessionCounters(Context context, LifeCounter[] counters) throws IOException {
            if (counters == null) {
                throw new IllegalArgumentException("Array of life counters cannot be null");
            }
            
            LifeCounterContainer[] containers = new LifeCounterContainer[counters.length];
            for (int idx = 0; idx < counters.length; idx++) {
                containers[idx] = new LifeCounterContainer(counters[idx].getName(), counters[idx].getLifeTotal(), counters[idx].getPoisonCounter());
            }
            
            LocalPersistence.writeObjectToFile(context, containers, LIFE_COUNTER_PREFERENCES_FILE);
        }
    }
}
